const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


// 1. Get all items that are available 

// console.log('...............test1..................')


const availableItems = items.filter(item => item.available == true)
console.log(availableItems)

// 2. Get all items containing only Vitamin C.

// console.log('...............test2..................')


const vitaminCItems = items.filter(item => item.contains == "Vitamin C")
console.log(vitaminCItems)


// 3. Get all items containing Vitamin A.

// console.log('...............test3..................')


const vitaminAItems = items.filter(item => item.contains.includes("Vitamin A"))
console.log(vitaminAItems)



/*
4. Group items based on the Vitamins that they contain in the following format:
{
    "Vitamin C": ["Orange", "Mango"],
    "Vitamin K": ["Mango"],
    and so on for all items and all Vitamins.
}
*/

// console.log('...............test4..................')


const groupVitaminsAndName = items.reduce((output, item) => {
    vitamins = item.contains.split(", ")

    vitamins.map((vitamin) => {

        if (output[vitamin]) {

            output[vitamin].push(item.name)

        } else {
            output[vitamin] = [item.name]
        }

    },)
    return output
}, {})

console.log(groupVitaminsAndName)


/*
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/
// console.log('...............test5..................')

const sortItemsByNumberOfVitamins = items.sort((item1, item2) => (item1.contains.split(",").length) - (item2.contains.split(",").length)
)
console.log(sortItemsByNumberOfVitamins)

